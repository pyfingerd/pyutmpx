#!/usr/bin/make -f
# Simple aliases for the setup.py file.

all:
	@./setup.py build
mostlyclean:
	@./setup.py clean
clean:
	@rm -rf build
re: clean all

test:
	@./setup.py test

install:
	@./setup.py install
install-user:
	@./setup.py install --user
dist:
	@./setup.py sdist

.PHONY: all mostlyclean clean re test install install-user dist
# End of file.
