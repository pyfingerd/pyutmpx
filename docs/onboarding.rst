Onboarding
==========

You're a new user trying to figure out what you can and cannot do with
pyutmpx, and you're willing to experiment? You're at the right place!
In this section, you will be able to install, run and start tweaking
pyutmpx to better suit your needs.

.. toctree::

	onboarding/installing
	onboarding/trying
