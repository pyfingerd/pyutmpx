API Reference
=============

.. py:module:: pyutmpx

If you are looking for information on a specific function, class or method,
this part of the documentation is for you.

utmp entry types
----------------

.. py:data:: EMPTY: int = 0

.. py:data:: BOOT_TIME: int = 1

.. py:data:: SHUTDOWN_TIME: int = 10

.. py:data:: OLD_TIME: int = 2

.. py:data:: NEW_TIME: int = 3

.. py:data:: USER_PROCESS: int = 4

.. py:data:: INIT_PROCESS: int = 5

.. py:data:: LOGIN_PROCESS: int = 6

.. py:data:: DEAD_PROCESS: int = 7

.. py:data:: RUN_LEVEL: int = 8

.. py:data:: ACCOUNTING: int = 9

Entry format
------------

.. py:class:: exit_status

	Exit status representation for dead process entries in utmp
	and related databases, as described by the
	``struct exit_status`` type defined in ``<utmp.h>``.

	.. py:attribute:: termination: int

		The process termination status, i.e. whether it has exited
		normally using exit(2) or if it has been terminated
		using a signal.

		This field is populated using the ``e->e_termination`` field.
		Refer to ``<utmp.h>`` and wait(2) for more information.

	.. py:attribute:: exit: int

		The process exit status, i.e. the code returned by the
		process in the case it has called exit(2).

		This field is populated using the ``e->e_exit`` field.
		Refer to ``<utmp.h>`` and wait(2) for more information.

.. py:class:: entry

	utmp or lastlog entry representation, as described by either the
	``struct utmp`` type defined in ``<utmp.h>``, the ``struct utmpx``
	type defined in ``<utmpx.h>``, or the ``struct lastlog`` type defined
	in ``<utmp.h>``.

	.. py:attribute:: type: int

		The type of the entry, amongst the following constants:

		:py:data:`pyutmpx.EMPTY`
			No valid user accounting information.

		:py:data:`pyutmpx.BOOT_TIME`
			Identifies time of system boot.

		:py:data:`pyutmpx.SHUTDOWN_TIME`
			Identifies time of system shutdown.

		:py:data:`pyutmpx.OLD_TIME`
			Identifies time when system clock changed.

		:py:data:`pyutmpx.NEW_TIME`
			Identifies time after system clock changed.

		:py:data:`pyutmpx.USER_PROCESS`
			Identifies a process.

		:py:data:`pyutmpx.INIT_PROCESS`
			Identifies a process spawned by the init process.

		:py:data:`pyutmpx.LOGIN_PROCESS`
			Identifies a session leader of a logged-in user.

		:py:data:`pyutmpx.DEAD_PROCESS`
			Identifies a session leader who has exited.

		:py:data:`pyutmpx.RUN_LEVEL`
			Identifies a change in system run-level; refer to init(1)
			for more information.

		:py:data:`pyutmpx.ACCOUNTING`
			No information available.

		This field is populated using the ``ut->ut_type`` field,
		when available.

	.. py:attribute:: time: datetime.datetime

		The time at which the entry was added to the database, as
		a datetime using the UTC timezone.

		This field is populated using the ``ut->ut_tv``,
		``ut->ut_time``, ``ut->ut_xtime`` or ``ll->ll_time`` field,
		when available.

	.. py:attribute:: user: bytes

		The login name of the user involved in the event.

		This field is populated using the ``ut->ut_user`` or
		``ut->ut_name`` fields.

	.. py:attribute:: uid: int

		The numerical identifier of the user involved in the event.

		This field is populated using the offset of the entry in
		a lastlog file.

	.. py:attribute:: line: bytes

		The line or device on which the event has occurred.

		This field is populated using the ``ut->ut_line`` or
		the ``ll->ll_line`` field.

	.. py:attribute:: host: bytes

		The host from which the event has occurred.

		The name of the remote host from which the event has occurred
		in the case of remote logins, or the kernel version for other
		system-related events.

		This field is populated using the ``ut->ut_host`` or
		the ``ll->ll_host`` field, when available.

	.. py:attribute:: addr: bytes

		The IPv4 or IPv6 address of the host in case of remote logins,
		as a string.

		This field is populated using the ``ut->ut_addr_v6`` or
		``ut->ut_addr`` fields, when available.

	.. py:attribute:: pid: int

		The process identifier (pid), for process or session leader
		related events.

		This field is populated using the ``ut->ut_pid`` field,
		when available.

	.. py:attribute:: sid: int

		The session identifier (sid) of the process, used for
		windowing.

		This field is populated using the ``ut->ut_session`` field,
		when available. Refer to getsid(2) for more information.

	.. py:attribute:: exit: exit_status

		The exit status of the process or session leader on
		dead process events, as a :py:class:`exit_status`
		instance.

		This field is populated using the ``ut->ut_exit`` field,
		when available.

	.. py:attribute:: id: bytes

		The unspecified initialization process identifier.

		This field is populated using the ``ut->ut_id`` field,
		when available.

Classes abstracting the databases
---------------------------------

.. py:class:: utmp

	An iterable read-only view of the utmp database, yielding
	:py:class:`entry` instances.

.. py:class:: wtmp

	An iterable read-only view of the wtmp (historical utmp) database
	when available, yielding :py:class:`entry` instances.

.. py:class:: btmp

	An iterable read-only view of the btmp (bad logins) database
	when available, yielding :py:class:`entry` instances.

.. py:class:: lastlog

	An iterable read-only view of the lastlog database, yielding
	:py:class:`entry` instances.
