/* ****************************************************************************
 * pyutmpx.h -- pyutmpx module header.
 * Copyright (C) 2017-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
 *
 * This file is part of the pyutmpx Python 3.x module, which is MIT-licensed.
 * ************************************************************************* */
#ifndef  PYUTMPX_H
# define PYUTMPX_H 2018020602
# define PY_SSIZE_T_CLEAN 1
# include <Python.h>
# include <pyerrors.h>
# include <structmember.h>
# include <netinet/in.h>
# include <arpa/inet.h>

/* ---
 * System-specific mess.
 * ---
 * Let's determine platform capabilities using feature macros.
 *
 * Macros defined here:
 *
 * PYUTMPX_UTMP_PATH, PYUTMPX_WTMP_PATH, PYUTMPX_BTMP_PATH
 *     Path of the utmp, wtmp or btmp database file using ``struct utmp``
 *     as its entry type.
 *
 * PYUTMPX_LASTLOG_PATH
 *     Path of the lastlog database file using ``struct lastlog`` as its
 *     entry type.
 *
 * PYUTMPX_UTMPX_SELECT, PYUTMPX_WTMPX_SELECT, PYUTMPX_BTMPX_SELECT,
 * PYUTMPX_LASTLOGX_SELECT
 *     File selecting code for initializing the standard <utmpx.h> functions
 *     using non-standardized methods.
 *
 * Note that priorities will be the following, for every database:
 *
 *  - If the historic path is defined for a given database, we will attempt at
 *    reading the database with the historic structure.
 *  - If the select code for <utmpx.h> functions is provided, we will attempt
 *    at reading the database using these functions. */

# if defined(__linux__)
#  include <utmp.h>

/* Linux paths for utmp, wtmp, btmp and lastlog. */

#  define PYUTMPX_UTMP_PATH    _PATH_UTMP
#  define PYUTMPX_WTMP_PATH    _PATH_WTMP
#  ifdef _PATH_BTMP
#   define PYUTMPX_BTMP_PATH   _PATH_BTMP
#  else
#   define PYUTMPX_BTMP_PATH    "/var/log/btmp"
#  endif
#  define PYUTMPX_LASTLOG_PATH _PATH_LASTLOG

/* Linux utmp structure and available elements. */

#  define PYUTMPX_HAS_UTMP_ID      1
#  define PYUTMPX_HAS_UTMP_TYPE    1
#  define PYUTMPX_HAS_UTMP_ADDR_V6 1
#  define PYUTMPX_HAS_UTMP_EXIT    1
#  define PYUTMPX_HAS_UTMP_PID     1
#  define PYUTMPX_HAS_UTMP_SESSION 1
#  define PYUTMPX_HAS_UTMP_TIMEVAL 1

# elif defined(__OpenBSD__)
#  include <utmp.h>

/* OpenBSD paths for utmp, wtmp and lastlog. */

#  define PYUTMPX_UTMP_PATH    _PATH_UTMP
#  define PYUTMPX_WTMP_PATH    _PATH_WTMP
#  define PYUTMPX_LASTLOG_PATH _PATH_LASTLOG

#  define ut_user ut_name

# elif defined(__FreeBSD__)
#  include <utmpx.h>

#  define PYUTMPX_UTMPX_SELECT \
	setutxdb(UTXDB_ACTIVE, NULL);
#  define PYUTMPX_WTMPX_SELECT \
	setutxdb(UTXDB_LOG, NULL);
#  define PYUTMPX_LASTLOGX_SELECT \
	setutxdb(UTXDB_LASTLOGIN, NULL);

#  define PYUTMPX_HAS_UTMPX_HOST 1
# endif

/* ---
 * General definitions.
 * --- */

/* General definitions. */

# if defined(PYUTMPX_UTMP_PATH) || defined(PYUTMPX_UTMPX_SELECT)
#  define PYUTMPX_HAS_UTMP 1
# endif

# if defined(PYUTMPX_WTMP_PATH) || defined(PYUTMPX_WTMPX_SELECT)
#  define PYUTMPX_HAS_WTMP 1
# endif

# if defined(PYUTMPX_BTMP_PATH) || defined(PYUTMPX_BTMPX_SELECT)
#  define PYUTMPX_HAS_BTMP 1
# endif

# if defined(PYUTMPX_LASTLOG_PATH) || defined(PYUTMPX_LASTLOGX_SELECT)
#  define PYUTMPX_HAS_LASTLOG 1
# endif

/* Platform specific functions for gathering entries. */

struct pyutmpx_exit_status {
	int *termination_code;
	int *status_code;
};

struct pyutmpx_node {
	struct pyutmpx_node *next;

	struct timeval *time;
	struct pyutmpx_exit_status *exit;

	int *type;

	char *id;
	char *user;
	char *host;
	char *line;
	char *addr;

	unsigned long *pid;
	unsigned long *sid;
	unsigned long *uid;

	Py_ssize_t id_size;
	Py_ssize_t user_size;
	Py_ssize_t host_size;
	Py_ssize_t line_size;
	Py_ssize_t addr_size;

	unsigned long data[]; /* For alignment purposes. */
};

# if defined(PYUTMPX_HAS_UTMP) && PYUTMPX_HAS_UTMP
extern int pyutmpx_get_utmp_nodes(struct pyutmpx_node **nodep);
# endif

# if defined(PYUTMPX_HAS_WTMP) && PYUTMPX_HAS_WTMP
extern int pyutmpx_get_wtmp_nodes(struct pyutmpx_node **nodep);
# endif

# if defined(PYUTMPX_HAS_BTMP) && PYUTMPX_HAS_BTMP
extern int pyutmpx_get_btmp_nodes(struct pyutmpx_node **nodep);
# endif

# if defined(PYUTMPX_HAS_LASTLOG) && PYUTMPX_HAS_LASTLOG
extern int pyutmpx_get_lastlog_nodes(struct pyutmpx_node **nodep);
# endif

/* Common utilities defined in utils.c. */

extern void pyutmpx_put_str(char **ps, size_t *pn, char const *s);
extern void pyutmpx_put_repr(char **ps, size_t *pn, PyObject *o);

extern Py_ssize_t pyutmpx_get_len(char const *s, Py_ssize_t n);

extern PyObject *pyutmpx_build_datetime(struct timeval const *tv);
extern PyObject *pyutmpx_build_entry(struct pyutmpx_node const *node);

/* Common API defined in entry_type.c:
 * Define a utmp entry (for utmp and wtmp files). */

# define PYUTMPX_EMPTY          0
# define PYUTMPX_BOOT_TIME      1
# define PYUTMPX_OLD_TIME       2
# define PYUTMPX_NEW_TIME       3
# define PYUTMPX_USER_PROCESS   4
# define PYUTMPX_INIT_PROCESS   5
# define PYUTMPX_LOGIN_PROCESS  6
# define PYUTMPX_DEAD_PROCESS   7
# define PYUTMPX_RUN_LEVEL      8
# define PYUTMPX_ACCOUNTING     9
# define PYUTMPX_SHUTDOWN_TIME 10

extern PyTypeObject pyutmpx_exit_status_type;
extern PyTypeObject pyutmpx_entry_type;

/* Setup and un-setup functions for all modules. */

extern int pyutmpx_init_utils(PyObject *module);
extern void pyutmpx_exit_utils(void);

extern int pyutmpx_init_exit_status_type(PyObject *module);
extern void pyutmpx_exit_exit_status_type(void);

extern int pyutmpx_init_entry_type(PyObject *module);
extern void pyutmpx_exit_entry_type(void);

# if defined(PYUTMPX_HAS_UTMP) && PYUTMPX_HAS_UTMP
extern int pyutmpx_init_utmp_type(PyObject *module);
extern void pyutmpx_exit_utmp_type(void);
# endif

# if defined(PYUTMPX_HAS_WTMP) && PYUTMPX_HAS_WTMP
extern int pyutmpx_init_wtmp_type(PyObject *module);
extern void pyutmpx_exit_wtmp_type(void);
# endif

# if defined(PYUTMPX_HAS_BTMP) && PYUTMPX_HAS_BTMP
extern int pyutmpx_init_btmp_type(PyObject *module);
extern void pyutmpx_exit_btmp_type(void);
# endif

# if defined(PYUTMPX_HAS_LASTLOG) && PYUTMPX_HAS_LASTLOG
extern int pyutmpx_init_lastlog_type(PyObject *module);
extern void pyutmpx_exit_lastlog_type(void);
# endif

#endif /* PYUTMPX_H */
