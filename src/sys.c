/* ****************************************************************************
 * sys.c -- system-specific implementation.
 * Copyright (C) 2017-2021 Thomas "Cakeisalie5" Touhey <thomas@touhey.fr>
 *
 * This file is part of the pyutmpx Python 3.x module, which is MIT-licensed.
 * ************************************************************************* */

#include "pyutmpx.h"
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#if defined(PYUTMPX_UTMP_PATH) || \
	defined(PYUTMPX_WTMP_PATH) || \
	defined(PYUTMPX_BTMP_PATH)
# define UTMP_USED        1
# define STRUCT_UTMP_USED 1
#endif

#if defined(PYUTMPX_LASTLOG_PATH)
# define LASTLOG_USED 1
# define STRUCT_LASTLOG_USED 1
#endif

#if (!defined(PYUTMPX_UTMP_PATH) && defined(PYUTMPX_UTMPX_SELECT)) || \
	(!defined(PYUTMPX_WTMP_PATH) && defined(PYUTMPX_WTMPX_SELECT)) || \
	(!defined(PYUTMPX_BTMP_PATH) && defined(PYUTMPX_BTMPX_SELECT)) || \
	(!defined(PYUTMPX_LASTLOG_PATH) && defined(PYUTMPX_LASTLOGX_SELECT))
# define UTMPX_USED 1
# define STRUCT_UTMPX_USED 1
#endif

/* ---
 * ``ut->ut_type`` matching.
 * --- */

#if (defined(STRUCT_UTMP_USED)  && STRUCT_UTMP_USED) || \
	(defined(STRUCT_UTMPX_USED) && STRUCT_UTMPX_USED)

/* `get_utmp_type()`: return a pyutmpx constant out of a ut_type constant. */

static int get_utmp_type(int ut_type)
{
	switch (ut_type) {
# ifdef EMPTY
	case EMPTY:
		return PYUTMPX_EMPTY;
# endif
# ifdef BOOT_TIME
	case BOOT_TIME:
		return PYUTMPX_BOOT_TIME;
# endif
# ifdef OLD_TIME
	case OLD_TIME:
		return PYUTMPX_OLD_TIME;
# endif
# ifdef NEW_TIME
	case NEW_TIME:
		return PYUTMPX_NEW_TIME;
# endif
# ifdef USER_PROCESS
	case USER_PROCESS:
		return PYUTMPX_USER_PROCESS;
# endif
# ifdef INIT_PROCESS
	case INIT_PROCESS:
		return PYUTMPX_INIT_PROCESS;
# endif
# ifdef LOGIN_PROCESS
	case LOGIN_PROCESS:
		return PYUTMPX_LOGIN_PROCESS;
# endif
# ifdef DEAD_PROCESS
	case DEAD_PROCESS:
		return PYUTMPX_DEAD_PROCESS;
# endif
# ifdef RUN_LVL
	case RUN_LVL:
		return PYUTMPX_RUN_LEVEL;
# endif
# ifdef RUN_LEVEL
	case RUN_LEVEL:
		return PYUTMPX_RUN_LEVEL;
# endif
# ifdef ACCOUNTING
	case ACCOUNTING:
		return PYUTMPX_ACCOUNTING;
# endif
# ifdef SHUTDOWN_TIME
	case SHUTDOWN_TIME:
		return PYUTMPX_SHUTDOWN_TIME;
# endif
	default:
		return -1;
	}
}

#endif

/* ---
 * Node building from ``struct utmp`` instances.
 * --- */

#if defined(STRUCT_UTMP_USED) && STRUCT_UTMP_USED

/* ``struct utmp`` is used for at least one node building operation,
 * we want to implement a generic function to build it. */

struct utmp_data {
	struct timeval time;

# if defined(PYUTMPX_HAS_UTMP_EXIT) && PYUTMPX_HAS_UTMP_EXIT
	struct pyutmpx_exit_status exit;

	int exit_termination_code;
	int exit_status_code;
# endif

# if defined(PYUTMPX_HAS_UTMP_PID) && PYUTMPX_HAS_UTMP_PID
	unsigned long pid;
# endif
# if defined(PYUTMPX_HAS_UTMP_SESSION) && PYUTMPX_HAS_UTMP_SESSION
	unsigned long sid;
# endif

# if defined(PYUTMPX_HAS_UTMP_TYPE) && PYUTMPX_HAS_UTMP_TYPE
	int type;
# endif

	char data[];
};

/* ``build_utmp_node()``: build a node out of a ``struct utmp`` instance. */

static struct pyutmpx_node *build_utmp_node(struct utmp const *ent)
{
# if defined(PYUTMPX_HAS_UTMP_TYPE) && PYUTMPX_HAS_UTMP_TYPE
	int type;
# endif
# if defined(PYUTMPX_HAS_UTMP_ADDR_V6) && PYUTMPX_HAS_UTMP_ADDR_V6
	char formatted_addr[100];
	char const *addr;
# endif
	Py_ssize_t id_len, user_len, host_len, line_len, addr_len = 0;
	struct pyutmpx_node *node;
	size_t node_size;

	/* If the entry is all zeroes, ignore it. */

	{
		char const *start = (char const *)ent;
		char const *end = ((char const *)&ent[1]) - 1;
		int all_zeroes = 1;

		for (; start < end; start++) {
			if (!*start)
				continue;

			all_zeroes = 0;
			break;
		}

		if (all_zeroes)
			return NULL;
	}

	/* Get the entry type. */

# if defined(PYUTMPX_HAS_UTMP_TYPE) && PYUTMPX_HAS_UTMP_TYPE
	type = get_utmp_type(ent->ut_type);
	if (type < 0)
		return NULL;
# endif

	/* Prepare the address. */

# if defined(PYUTMPX_HAS_UTMP_ADDR_V6) && PYUTMPX_HAS_UTMP_ADDR_V6
	addr = NULL;

	if (ent->ut_addr_v6[1] || ent->ut_addr_v6[2] || ent->ut_addr_v6[3])
		addr = inet_ntop(AF_INET6, &ent->ut_addr_v6,
			formatted_addr, sizeof formatted_addr);
	else if (ent->ut_addr_v6[0])
		addr = inet_ntop(AF_INET, &ent->ut_addr_v6,
			formatted_addr, sizeof formatted_addr);

	if (!addr)
		addr = "";

	addr_len = strlen(addr);
# endif

	/* Get the lengths. */

# if defined(PYUTMPX_HAS_UTMP_ID) && PYUTMPX_HAS_UTMP_ID
	id_len = pyutmpx_get_len(ent->ut_id, 4);
# else
	id_len = 0;
# endif

	user_len = pyutmpx_get_len(ent->ut_user, sizeof ent->ut_user);
	host_len = pyutmpx_get_len(ent->ut_host, sizeof ent->ut_host);
	line_len = pyutmpx_get_len(ent->ut_line, sizeof ent->ut_line);

	/* Allocate the node. */

	node_size = sizeof(struct pyutmpx_node)
		+ sizeof(struct utmp_data)
		+ id_len + user_len + host_len + line_len + addr_len;
	node = malloc(node_size);
	if (!node)
		return NULL; /* We quit cowardly, pretenting nothing follows. */

	memset(node, 0, node_size);

	node->next = NULL;

	{
		struct utmp_data *data = (struct utmp_data *)&node->data;

# if defined(PYUTMPX_HAS_UTMP_TYPE) && PYUTMPX_HAS_UTMP_TYPE
		node->type = &data->type;
		data->type = type;
# endif

# if defined(PYUTMPX_HAS_UTMP_EXIT) && PYUTMPX_HAS_UTMP_EXIT
		if (type == PYUTMPX_DEAD_PROCESS) {
			node->exit = &data->exit;
			node->exit->termination_code = &data->exit_termination_code;
			node->exit->status_code = &data->exit_status_code;

			data->exit_termination_code = ent->ut_exit.e_termination;
			data->exit_status_code = ent->ut_exit.e_exit;
		}
# endif

# if defined(PYUTMPX_HAS_UTMP_PID) && PYUTMPX_HAS_UTMP_PID
		node->pid = &data->pid;
		data->pid = ent->ut_pid;
# endif

# if defined(PYUTMPX_HAS_UTMP_SESSION) && PYUTMPX_HAS_UTMP_SESSION
		node->sid = &data->sid;
		data->sid = ent->ut_session;
# endif

		node->time = &data->time;
# if defined(PYUTMPX_HAS_UTMP_TIMEVAL) && PYUTMPX_HAS_UTMP_TIMEVAL
		data->time.tv_sec = ent->ut_tv.tv_sec;
		data->time.tv_usec = ent->ut_tv.tv_usec;
# else
		data->time.tv_sec = ent->ut_time;
		data->time.tv_usec = 0;
# endif

		node->id = data->data;
		node->user = node->id + id_len;
		node->host = node->user + user_len;
		node->line = node->host + host_len;
		node->addr = node->line + line_len;

		node->id_size = id_len;
		node->user_size = user_len;
		node->host_size = host_len;
		node->line_size = line_len;
		node->addr_size = addr_len;

# if defined(PYUTMPX_HAS_UTMP_ID) && PYUTMPX_HAS_UTMP_ID
		if (id_len)
			memcpy(node->id, ent->ut_id, id_len);
# else
		node->id = NULL;
# endif

		if (user_len)
			memcpy(node->user, ent->ut_user, user_len);
		if (host_len)
			memcpy(node->host, ent->ut_host, host_len);
		if (line_len)
			memcpy(node->line, ent->ut_line, line_len);

# if defined(PYUTMPX_HAS_UTMP_ADDR_V6) && PYUTMPX_HAS_UTMP_ADDR_V6
		if (addr_len)
			memcpy(node->addr, addr, addr_len);
# else
		node->addr = NULL;
# endif
	}

	return (node);
}

#endif

/* ---
 * Node building from ``struct utmp`` instances.
 * --- */

#if defined(STRUCT_UTMPX_USED) && STRUCT_UTMPX_USED

/* ``struct utmpx`` is used for at least one node building operation,
 * we want to implement a generic function to build it. */

struct utmpx_data {
	struct timeval time;
	unsigned long pid;
	int type;

	char data[];
};

/* ``build_utmpx_node()``: build a node out of a ``struct utmpx`` instance. */

static struct pyutmpx_node *build_utmpx_node(struct utmpx const *ent)
{
	int type;
	Py_ssize_t id_len, user_len, host_len = 0, line_len;
	struct pyutmpx_node *node;
	size_t node_size;

	/* If the entry is all zeroes, ignore it. */

	{
		char const *start = (char const *)ent;
		char const *end = ((char const *)&ent[1]) - 1;
		int all_zeroes = 1;

		for (; start < end; start++) {
			if (!*start)
				continue;

			all_zeroes = 0;
			break;
		}

		if (all_zeroes)
			return NULL;
	}

	/* Get the entry type. */

	type = get_utmp_type(ent->ut_type);
	if (type < 0)
		return NULL;

	/* Get the lengths. */

	id_len = pyutmpx_get_len(ent->ut_id, 4);
	user_len = pyutmpx_get_len(ent->ut_user, sizeof ent->ut_user);
	line_len = pyutmpx_get_len(ent->ut_line, sizeof ent->ut_line);
# if defined(PYUTMPX_HAS_UTMPX_HOST) && PYUTMPX_HAS_UTMPX_HOST
	host_len = pyutmpx_get_len(ent->ut_host, sizeof ent->ut_host);
# endif

	/* Allocate the node. */

	node_size = sizeof(struct pyutmpx_node)
		+ sizeof(struct utmpx_data)
		+ id_len + user_len + host_len + line_len;
	node = malloc(node_size);
	if (!node)
		return NULL; /* We quit cowardly, pretenting nothing follows. */

	memset(node, 0, node_size);

	node->next = NULL;

	{
		struct utmpx_data *data = (struct utmpx_data *)&node->data;

		node->type = &data->type;
		data->type = type;

		node->pid = &data->pid;
		data->pid = ent->ut_pid;

		node->time = &data->time;
		data->time.tv_sec = ent->ut_tv.tv_sec;
		data->time.tv_usec = ent->ut_tv.tv_usec;

		node->id = data->data;
		node->user = node->id + id_len;
		node->host = node->user + user_len;
		node->line = node->host + host_len;

		node->id_size = id_len;
		node->user_size = user_len;
		node->host_size = host_len;
		node->line_size = line_len;

		if (id_len)
			memcpy(node->id, ent->ut_id, id_len);
		if (user_len)
			memcpy(node->user, ent->ut_user, user_len);
		if (line_len)
			memcpy(node->line, ent->ut_line, line_len);

# if defined(PYUTMPX_HAS_UTMPX_HOST) && PYUTMPX_HAS_UTMPX_HOST
		if (host_len)
			memcpy(node->host, ent->ut_host, host_len);
# else
		node->host = NULL;
# endif
	}

	return (node);
}

#endif

/* ---
 * Node building from ``struct lastlog`` instances.
 * --- */

#if defined(STRUCT_LASTLOG_USED) && STRUCT_LASTLOG_USED

struct lastlog_data {
	struct timeval time;
	unsigned long uid;

	char data[];
};

static struct pyutmpx_node *build_lastlog_node(unsigned long uid,
	struct lastlog const *ent)
{
	struct pyutmpx_node *node;
	Py_ssize_t host_len, line_len;
	size_t node_size;

	/* If the entry is all zeroes, ignore it. */

	{
		char const *start = (char const *)ent;
		char const *end = ((char const *)&ent[1]) - 1;
		int all_zeroes = 1;

		for (; start < end; start++) {
			if (!*start)
				continue;

			all_zeroes = 0;
			break;
		}

		if (all_zeroes)
			return NULL;
	}

	/* Get the length and start buildin'. */

	host_len = pyutmpx_get_len(ent->ll_host, UT_HOSTSIZE);
	line_len = pyutmpx_get_len(ent->ll_line, UT_LINESIZE);

	node_size = sizeof (struct pyutmpx_node)
		+ sizeof (struct lastlog_data) + host_len + line_len;
	node = malloc(node_size);
	if (!node)
		return 0; /* Escape cowardly. */

	memset(node, 0, node_size);

	{
		struct lastlog_data *data = (struct lastlog_data *)&node->data;

		node->next = NULL;

		node->uid = &data->uid;
		data->uid = uid;

		node->time = &data->time;
		node->time->tv_sec = ent->ll_time;
		node->time->tv_usec = 0;

		node->host = data->data;
		node->line = node->host + host_len;

		node->host_size = host_len;
		node->line_size = line_len;

		memcpy(node->host, ent->ll_host, host_len);
		memcpy(node->line, ent->ll_line, line_len);
	}

	return node;
}

#endif

/* ---
 * utmp, wtmp and btmp raw entries gathering.
 * --- */

#if defined(UTMP_USED) && UTMP_USED

static int get_utmp_nodes(struct pyutmpx_node **nodep, char const *path)
{
	struct pyutmpx_node *node, **inodep;
	struct utmp arr[500];
	char *arrp = (char *)&arr;
	size_t offset = 0;
	int fd;

	inodep = nodep;
	*nodep = NULL;

	fd = open(path, O_RDONLY, 0);
	if (fd < 0) {
		PyErr_SetFromErrnoWithFilename(PyExc_OSError, path);
		return -1;
	}

	while (1) {
		size_t arr_size, full_size;
		ssize_t read_size;

		read_size = read(fd, &arrp[offset],
			sizeof (arr) - offset);
		if (read_size < 0) {
			close(fd);
			while (*inodep) {
				node = *inodep;
				inodep = &node->next;
				free(node);
			}

			PyErr_SetFromErrnoWithFilename(PyExc_OSError, path);
			return -1;
		}

		if (!read_size)
			break; /* End of file! */

		full_size = read_size - read_size % sizeof (arr[0]);
		arr_size = full_size / sizeof (arr[0]);
		for (size_t i = 0; i < arr_size; i++) {
			node = build_utmp_node(&arr[i]);
			if (node) {
				*nodep = node;
				nodep = &node->next;
			}
		}

		read_size -= full_size;
		offset = 0;
		if (read_size) {
			memcpy(arrp, &arrp[full_size], read_size);
			offset = read_size;
		}
	}

	close(fd);

	if (offset) {
		while (*inodep) {
			node = *inodep;
			inodep = &node->next;
			free(node);
		}

		PyErr_SetString(PyExc_OSError,
			"Incomplete entry at end of file");
		return -1;
	}

	return 0;
}

# define UTMP_FILE_READER(FUNC_NAME, PATH) \
int FUNC_NAME(struct pyutmpx_node **nodep) \
{ \
	return get_utmp_nodes(nodep, PATH); \
}

# if defined(PYUTMPX_UTMP_PATH)
UTMP_FILE_READER(pyutmpx_get_utmp_nodes, PYUTMPX_UTMP_PATH)
# endif

# if defined(PYUTMPX_WTMP_PATH)
UTMP_FILE_READER(pyutmpx_get_wtmp_nodes, PYUTMPX_WTMP_PATH)
# endif

# if defined(PYUTMPX_BTMP_PATH)
UTMP_FILE_READER(pyutmpx_get_btmp_nodes, PYUTMPX_BTMP_PATH)
# endif
#endif

/* ---
 * utmpx entries gathering.
 * --- */

#if defined(UTMPX_USED) && UTMPX_USED

/* `get_utmpx_nodes()`: get nodes using the standard method defined
 * in <utmpx.h>. */

static int get_nodes_using_utmpx_h(struct pyutmpx_node **nodep,
	int required_type)
{
	struct pyutmpx_node *node;

	*nodep = NULL;

	setutxent();

	while (1) {
		struct utmpx *ent = getutxent();

		if (!ent)
			break;

		node = build_utmpx_node(ent);
		if (!node)
			continue ;

		if (required_type >= 0) {
			if (node->type
			 && required_type != *node->type)
				continue ;

			node->type = NULL;
		}

		*nodep = node;
		nodep = &node->next;
	}

	endutxent();

	return 0;
}

# if !defined(PYUTMPX_UTMP_PATH) && defined(PYUTMPX_UTMPX_SELECT)

int pyutmpx_get_utmp_nodes(struct pyutmpx_node **nodep)
{
	PYUTMPX_UTMPX_SELECT;

	return get_nodes_using_utmpx_h(nodep, -1);
}

# endif
# if !defined(PYUTMPX_WTMP_PATH) && defined(PYUTMPX_WTMPX_SELECT)

int pyutmpx_get_wtmp_nodes(struct pyutmpx_node **nodep)
{
	PYUTMPX_WTMPX_SELECT;

	return get_nodes_using_utmpx_h(nodep, -1);
}

# endif
# if !defined(PYUTMPX_BTMP_PATH) && defined(PYUTMPX_BTMPX_SELECT)

int pyutmpx_get_btmp_nodes(struct pyutmpx_node **nodep)
{
	PYUTMPX_BTMPX_SELECT;

	return get_nodes_using_utmpx_h(nodep, -1);
}

# endif
# if !defined(PYUTMPX_LASTLOG_PATH) && defined(PYUTMPX_LASTLOGX_SELECT)

int pyutmpx_get_lastlog_nodes(struct pyutmpx_node **nodep)
{
	PYUTMPX_LASTLOGX_SELECT;

	return get_nodes_using_utmpx_h(nodep, PYUTMPX_USER_PROCESS);
}

# endif
#endif

/* ---
 * lastlog entries gathering.
 * --- */

#if defined(LASTLOG_USED) && LASTLOG_USED

/* `pyutmpx_get_lastlog_nodes()`: gather last nodes. */

int pyutmpx_get_lastlog_nodes(struct pyutmpx_node **nodep)
{
	struct pyutmpx_node *node, **inodep;
	struct lastlog arr[500];
	char *arrp = (char *)&arr;
	size_t offset = 0;
	int fd;

	inodep = nodep;
	*nodep = NULL;

	fd = open(PYUTMPX_LASTLOG_PATH, O_RDONLY, 0);
	if (fd < 0) {
		PyErr_SetFromErrnoWithFilename(PyExc_OSError, PYUTMPX_LASTLOG_PATH);
		return -1;
	}

	for (unsigned long uid_start = 0;;) {
		size_t arr_size, full_size;
		ssize_t read_size;

		read_size = read(fd, &arrp[offset], sizeof (arr) - offset);
		if (read_size < 0) {
			close(fd);
			while (*inodep) {
				node = *inodep;
				inodep = &node->next;
				free(node);
			}

			PyErr_SetFromErrnoWithFilename(PyExc_OSError,
				PYUTMPX_LASTLOG_PATH);
			return -1;
		}

		if (!read_size)
			break; /* End of file! */

		full_size = read_size - read_size % sizeof (arr[0]);
		arr_size = full_size / sizeof (arr[0]);

		for (unsigned long uid_off = 0; uid_off < arr_size; uid_off++) {
			unsigned long uid = uid_start + uid_off;
			struct lastlog *ll = &arr[uid_off];

			node = build_lastlog_node(uid, ll);
			if (node) {
				*nodep = node;
				nodep = &node->next;
			}
		}

		read_size -= full_size;
		offset = 0;
		if (read_size) {
			memcpy(arrp, &arrp[full_size], read_size);
			offset = read_size;
		}

		uid_start += arr_size;
	}

	close(fd);

	if (offset) {
		while (*inodep) {
			node = *inodep;
			inodep = &node->next;
			free(node);
		}

		PyErr_SetString(PyExc_OSError, "Incomplete entry at end of file");
		return -1;
	}

	return 0;
}

#endif
